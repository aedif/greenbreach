import copy
import itertools


class Solver:
    def __init__(self, problem, targets, breach):
        self.problem = problem
        self.problem_w = len(problem[0])
        self.problem_h = len(problem)

        self.targets = targets
        self.breach = breach

    def generate_all_chains(self):
        """ Generates all possible chains of codes within the breach limit.  """
        all_chains = []
        for l in range(1, len(self.targets) + 1):
            for subset in itertools.permutations(self.targets, l):
                chain = subset[0]
                for i in range(1, len(subset)):
                    chain = self.__merge(chain, subset[i])
                if len(chain) <= self.breach:
                    all_chains.append(chain)
        return all_chains

    def __evaluate_solutions(self, solutions):
        """Evaluates each solution with a score and returns the best one. Score
        is based on how many target sequences are within it, with the next 2
        targets combined in the list provided to the solver being worth less than
        the next.

        e.g. targets = [target1, target2, target3]
             scores  = [1,3,5]
             if solution contains both target2 and target3
             total score = 8
        """
        n_targets = len(self.targets)
        target_scores = [2 * i + 1 for i in range(n_targets)]
        evaluated_solutions = []
        for solution in solutions:
            score = 0
            for i in range(n_targets):
                merged = self.__merge(solution[0], self.targets[i])
                if solution[0] == merged:
                    score += target_scores[i]
            evaluated_solutions.append((score, solution[0], solution[1]))

        best_solution = max(evaluated_solutions, key=lambda e: e[0])
        return best_solution

    def find_solution(self):
        all_chains = self.generate_all_chains()

        selected = [
            [False for x in range(self.problem_w)] for y in range(self.problem_h)
        ]
        all_solutions = []
        for chain in all_chains:
            solutions = []
            self.target = chain
            for i in range(self.problem_w):
                solution = self.search(copy.deepcopy(selected), i, 0, 0, True, chain)
                if solution:
                    solutions.append(solution)
            if solutions:
                all_solutions.append((chain, min(solutions, key=len)))
        return self.__evaluate_solutions(all_solutions)

    def search(self, selected, x, y, steps, y_step, target=[], curr_sol=[]):
        if selected[y][x] or not target:
            return None

        selected[y][x] = True
        curr_sol = curr_sol + [(x, y)]
        steps += 1

        if self.problem[y][x] == target[0]:
            target = target[1:]
        else:
            target = self.target

        if steps <= self.breach and not target:
            return curr_sol
        elif steps > self.breach or steps + len(target) > self.breach:
            return None

        best_solution = None
        best_sol_len = 999
        if y_step:
            for yt in range(0, self.problem_h):
                solution = self.search(
                    copy.deepcopy(selected), x, yt, steps, False, target, curr_sol
                )
                if solution and len(solution) < best_sol_len:
                    best_solution = solution
                    best_sol_len = len(solution)
        else:
            for xt in range(0, self.problem_w):
                solution = self.search(
                    copy.deepcopy(selected), xt, y, steps, True, target, curr_sol
                )
                if solution and len(solution) < best_sol_len:
                    best_solution = solution
                    best_sol_len = len(solution)

        return best_solution

    def __merge(self, seq1, seq2):
        """Merges overlapping sequences together.
        e.g. [1,2,3,4] + [3,4,5] = [1,2,3,4,5]"""
        seq1_l = len(seq1)
        seq2_l = len(seq2)
        for i in range(seq1_l):
            for j in range(seq2_l):
                if i + j >= seq1_l:
                    return seq1[0:i] + seq2
                if seq1[i + j] != seq2[j]:
                    break
                elif j == seq2_l - 1:
                    return seq1[0:i] + seq2 + seq1[i + seq2_l :]
        return seq1 + seq2

    def print_solution(self, solution):
        print("Sequence: " + str(solution[1]))
        print("Assigned score: " + str(solution[0]))

        grid = [["_" for i in range(self.problem_w)] for j in range(self.problem_h)]

        i = 0
        for x, y in solution[2]:
            grid[y][x] = str(i)
            i += 1

        for row in grid:
            print(row)


# PROBLEM = [
#     ["55", "BD", "7A", "BD", "55", "7A", "1C"],
#     ["E9", "7A", "7A", "1C", "1C", "55", "55"],
#     ["7A", "55", "55", "BD", "FF", "BD", "7A"],
#     ["55", "BD", "BD", "55", "7A", "FF", "BD"],
#     ["E9", "1C", "55", "55", "7A", "FF", "7A"],
#     ["BD", "BD", "1C", "E9", "1C", "E9", "1C"],
#     ["55", "BD", "7A", "1C", "55", "BD", "E9"],
# ]
# TARGETS = [["55", "E9", "1C"], ["E9", "55", "BD"], ["BD", "55", "BD", "55"]]
# BREACH = 7

# solver = Solver(PROBLEM, TARGETS, BREACH)
# solution = solver.find_solution()
# solver.print_solution(solution)