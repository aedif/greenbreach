# GreenBreach #

An automatic solver for the Cyberpunk 2077's breach minigame.

## Example ##

![Sample](https://bitbucket.org/aedif/greenbreach/raw/a5e5679eee39208e86fa27de0394b5984cc1ebde/proj_images/example_breach.png)

## Set up ##

To install clone the repo and run 'install.bat'. It should download the dependencies and create a virtual env from which the script will be run. 

## Running the script ##

## breach.bat ##
Pass the path of the screenshot as an argument. It will run using the virtual enviroment setup using 'install.bat'.
e.g. breach.bat "C:\path\to\screen\cap\img.png"

## breach.py ##
Assuming dependencies are met you can run the script directly.
e.g. python breach.py "C:\path\to\screen\cap\img.png"

## GreenShot ##
To run it via GreenShot you'll need to set it up to pass temp images created by it to 'breach.bat'.

Instruction for install and set up can be found [HERE](https://getgreenshot.org/2013/01/28/how-to-use-the-external-command-plugin-to-send-screenshots-to-other-applications/). 
