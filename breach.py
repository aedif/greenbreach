import sys
import os
import cv2
import numpy as np
import imutils
from solver import Solver

# Order of the codes in codes.png
code_arr = ["BD", "E9", "55", "7A", "1C", "FF"]
# Colors to identify them with
colors = [
    (227, 65, 16),
    (247, 229, 32),
    (82, 247, 32),
    (20, 237, 245),
    (245, 61, 255),
    (82, 152, 255),
]
# Specific thresholds for each code
# thresholds = [0.85, 0.85, 0.85, 0.83, 0.85, 0.83]

print("RUNNING BREACH")

TOP_PATH = os.path.dirname(os.path.realpath(__file__))

print("\tLOADING IMAGES")
im_path = f"{TOP_PATH}/test_images/r2.png"
if len(sys.argv) >= 2:
    im_path = sys.argv[1]

codes_path = f"{TOP_PATH}/test_images/codes.png"
buffer_path = f"{TOP_PATH}/test_images/buffer.png"

im = cv2.imread(im_path)


def color_range_mask(img, lw_col, up_col):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lower_col = np.array(lw_col)
    upper_col = np.array(up_col)

    mask = cv2.inRange(hsv, lower_col, upper_col)
    return mask


def codes_to_list(codes, scale=1.0):
    CODE_H = 50
    CODE_W = 57

    code_list = []
    (codes_h, codes_w) = codes.shape[:2]
    x = 0
    y = 0

    while x < codes_w and y < codes_h:
        code = codes[y : y + CODE_H, x : x + CODE_W]

        x += CODE_W
        if not x < codes_w:
            x = 0
            y += CODE_H

        if np.mean(code) == 0:
            continue

        if scale != 1.0:
            code = imutils.resize(code, width=int(code.shape[1] * scale))
        code_list.append(code)

    return code_list


def find_scale(codes, grayImage):
    highest_total = 0
    best_scale = 1.0

    for scale in np.linspace(0.2, 1.0, 20)[::-1]:
        # resize the image according to the scale, and keep track
        # of the ratio of the resizing
        resized = imutils.resize(grayImage, width=int(grayImage.shape[1] * scale))

        # detect edges in the resized, grayscale image and apply template
        # (_, binimg) = cv2.threshold(
        #     resized, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU
        # )

        max_val_total = 0.0

        for code in codes:
            # if code/template larger than the actual image skip
            (code_h, code_w) = code.shape[:2]
            if resized.shape[0] < code_h or resized.shape[1] < code_w:
                continue

            # detect edges in the resized, grayscale image and apply template
            # matching to find the template in the image
            result = cv2.matchTemplate(resized, code, cv2.TM_CCOEFF_NORMED)
            (_, maxVal, _, _) = cv2.minMaxLoc(result)

            max_val_total += maxVal

        if highest_total < max_val_total:
            highest_total = max_val_total
            best_scale = scale

    return best_scale


def find_matches(codes, img, grayImage, scale, threshold=0.7, colors=colors):
    # detect edges in the resized, grayscale image and apply template
    resized = imutils.resize(grayImage, width=int(grayImage.shape[1] * scale))
    # (_, binimg) = cv2.threshold(resized, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    # cv2.imshow("binimg", binimg)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    r = img.shape[1] / float(resized.shape[1])

    index = 0

    found_codes = []

    for code in codes:
        (code_h, code_w) = code.shape[:2]

        # detect edges in the resized, grayscale image and apply template
        # matching to find the template in the image
        result = cv2.matchTemplate(resized, code, cv2.TM_CCOEFF_NORMED)

        # threshold = thresholds[index]  # temp, code specific

        match_locations = np.where(result >= threshold)

        for (ax, ay) in zip(match_locations[1], match_locations[0]):

            (startX, startY) = (int(ax * r), int(ay * r))
            (endX, endY) = (int((ax + code_w) * r), int((ay + code_h) * r))

            # print(f"startX {startX} startY  {startY}")
            # print(f"endX {endX} endY  {endY}")

            found_codes.append((startX, startY, code_arr[index]))

            cv2.rectangle(img, (startX, startY), (endX, endY), colors[index], 2)

        index += 1
    return found_codes


def dist2(p1, p2):
    return (p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2


def reduce(found_codes):
    n = len(found_codes)
    duplicate = [False] * n
    reduced_found_sequences = []
    for i in range(n):
        x1, y1, lbl = found_codes[i]
        if not duplicate[i]:
            reduced_found_sequences.append(found_codes[i])
            for j in range(i + 1, n):
                x2, y2, lbl2 = found_codes[j]
                if dist2((x1, y1), (x2, y2)) < 25:
                    duplicate[j] = True
                    if lbl != lbl2:
                        print(f"OOPS, not the same labels {lbl} {lbl2}")
    return reduced_found_sequences


def to_2d_arr(codes):
    matrix = []
    n = len(codes)
    arranged = [False] * n

    for i in range(n):
        if not arranged[i]:
            _, y1, _ = codes[i]
            arranged[i] = True
            row = [codes[i]]
            for j in range(i + 1, n):
                _, y2, _ = codes[j]
                if (y1 - y2) ** 2 < 25:
                    row.append(codes[j])
                    arranged[j] = True
            row.sort(key=lambda tup: tup[0])
            matrix.append(row)
    matrix.sort(key=lambda lst: lst[0][1])

    for i in range(len(matrix)):
        row = [p[2] for p in matrix[i]]
        matrix[i] = row

    return matrix


def pretty_print(matrix):
    for row in matrix:
        print(row)
    print()


def draw_save_img(
    code_list, code_sol_list, tim, grayImage, grayImage2, best_scale, best_scale2
):
    i = 0
    for t in np.linspace(0.2, 1.0, 20)[::-1]:
        tim = im.copy()
        find_matches(code_list, tim, grayImage, best_scale, t)
        find_matches(code_sol_list, tim, grayImage2, best_scale2, t)

        cv2.imwrite(f"{TOP_PATH}/test_images/gif/{i}.png", tim)
        i += 1


def draw_display_sol(solution, solver):
    sol_img_height = solver.problem_h * 50 + 50 + 25 * len(solver.targets)
    sol_img = np.zeros((sol_img_height, solver.problem_w * 50, 3), np.uint8)

    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 0.8
    fontColor = (255, 255, 255)
    lineType = 2

    color = (0, 255, 0)
    thickness = 2

    # draw lines
    for i in range(len(solution[2]) - 1):
        x, y = solution[2][i]
        x2, y2 = solution[2][i + 1]
        cv2.line(
            sol_img,
            ((x + 1) * 50 - 25, (y + 1) * 50 - 25),
            ((x2 + 1) * 50 - 25, (y2 + 1) * 50 - 25),
            (0, 150, 0),
            thickness,
        )

    # draw rectangles
    first = True
    for x, y in solution[2]:
        x1 = (x + 1) * 50 - 42
        y1 = (y + 1) * 50 - 42
        x2 = (x + 1) * 50 - 6
        y2 = (y + 1) * 50 - 6
        cv2.rectangle(
            sol_img,
            (x1, y1),
            (x2, y2),
            (0, 0, 0),
            -1,
        )
        if first:
            first = False
            cv2.rectangle(
                sol_img,
                (x1, y1),
                (x2, y2),
                (0, 0, 150),
                -1,
            )
        cv2.rectangle(
            sol_img,
            (x1, y1),
            (x2, y2),
            color,
            thickness,
        )
    # draw text
    for x in range(solver.problem_w):
        for y in range(solver.problem_h):
            cv2.putText(
                sol_img,
                solver.problem[y][x],
                (x * 50 + 10, y * 50 + 35),
                font,
                fontScale,
                fontColor,
                lineType,
            )

    # draw breach info
    cv2.putText(
        sol_img,
        f"BUFFER size used: {solver.breach}",
        (10, solver.problem_h * 50 + 30),
        font,
        0.6,
        fontColor,
        lineType,
    )

    # draw sequences
    for i in range(len(solver.targets)):
        cv2.putText(
            sol_img,
            str(solver.targets[i]),
            (10, solver.problem_h * 50 + 40 + (i + 1) * 20),
            font,
            0.5,
            fontColor,
            lineType,
        )

    cv2.imshow("Solution", sol_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


tim = im.copy()

print("\tPROCESSING CODE TEMPLATES")
# load breach code templates and extract just the required color range
codes = cv2.imread(codes_path)
codes = color_range_mask(codes, [30, 97, 0], [54, 255, 255])

# load buffer template
buffer = cv2.imread(buffer_path)
buffer = color_range_mask(buffer, [30, 97, 0], [54, 255, 255])

print("\tAPPLYING MASKS IMAGES")
# Find code matrix codes
grayImage = color_range_mask(im, [30, 97, 0], [54, 255, 255])
best_scale = find_scale(codes_to_list(codes), grayImage)
print("Scale found: " + str(best_scale))
code_list = codes_to_list(codes)
print("\tLOOKING FOR MATCHES")
found_code_matrix = find_matches(code_list, tim, grayImage, best_scale, 0.83)

# Find buffers
# buffer_list = codes_to_list(buffer, best_scale)
# found_buffers = find_matches(
#     buffer_list, tim, grayImage, best_scale, 0.6, colors=[(0, 0, 255)]
# )

# Find sequence required codes
code_sol_list = codes_to_list(codes, 0.8)
grayImage2 = color_range_mask(im, [0, 0, 69], [179, 8, 255])
best_scale2 = find_scale(code_sol_list, grayImage2)
found_sequences = find_matches(code_sol_list, tim, grayImage2, best_scale2, 0.78)

# draw_save_img(
#     code_list, code_sol_list, tim, grayImage, grayImage2, best_scale, best_scale2
# )

print("\tPREPARING MATCHES FOR SOLVER")
found_code_matrix = to_2d_arr(reduce(found_code_matrix))
found_sequences = to_2d_arr(reduce(found_sequences))
# found_buffers = reduce(found_buffers)

pretty_print(found_code_matrix)
pretty_print(found_sequences)
# num_buffers = len(found_buffers)
num_buffers = 7  # inconsistent result looking for buffers.. setting to 7 instead
print(f"BUFFERS: {num_buffers}")

print("\tSOLVING")
solver = Solver(found_code_matrix, found_sequences, num_buffers)
solution = solver.find_solution()

draw_display_sol(solution, solver)
# for x, y in solution[2]:
#     print(found_code_matrix[y][x])

# cv2.imwrite(f"./test_images/gif/test.png", tim)

# cv2.imshow("Image", im)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
